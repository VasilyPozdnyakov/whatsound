# First aid kit

## About the Project

The project aims to create Android app for small children, where user can learn names of animal and sounds they produce in a form of a game.

## Built With

The project is created with Java using Android Studio.