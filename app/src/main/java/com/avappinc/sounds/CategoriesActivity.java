package com.avappinc.sounds;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import static com.avappinc.sounds.InitialActivity.SECTION_NAME;
import static com.avappinc.sounds.InitialActivity.activityElementsLists;

public class CategoriesActivity extends AppCompatActivity {

    private String sectionName;
    private ImageView animal, nature, music, home, car, random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        animal = findViewById(R.id.animalView);
        random = findViewById(R.id.allView);
        car = findViewById(R.id.carView);
        nature = findViewById(R.id.natureView);
        music = findViewById(R.id.musicView);
        home = findViewById(R.id.homeView);
        imageClick();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.INSTANCE.pauseMainMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Music.INSTANCE.playMusic(this);
    }

    @Override
    public void finish() {
        super.finish();
        activityElementsLists.saveGuessList(this);
    }

    public void openCategoryActivity() {
        Intent intent = new Intent(this, CategoryActivity.class);
        intent.putExtra(SECTION_NAME, sectionName);
        startActivity(intent);
    }

    public void imageClick() {
        animal.setOnClickListener(
                v -> {
                    Music.INSTANCE.pauseMainMusic();
                    sectionName = Category.ANIMAL.name();
                    openCategoryActivity();
                }
        );

        car.setOnClickListener(
                v -> {
                    Music.INSTANCE.pauseMainMusic();
                    sectionName = Category.CAR.name();
                    openCategoryActivity();
                }
        );
        home.setOnClickListener(
                v -> {
                    Music.INSTANCE.pauseMainMusic();
                    sectionName = Category.HOME.name();
                    openCategoryActivity();
                }
        );
        nature.setOnClickListener(
                v -> {
                    Music.INSTANCE.pauseMainMusic();
                    sectionName = Category.NATURE.name();
                    openCategoryActivity();
                }
        );
        random.setOnClickListener(
                v -> {
                    Music.INSTANCE.pauseMainMusic();
                    sectionName = Category.RANDOM.name();
                    openCategoryActivity();
                }
        );
        music.setOnClickListener(
                v -> {
                    Music.INSTANCE.pauseMainMusic();
                    sectionName = Category.MUSIC.name();
                    openCategoryActivity();
                }
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
