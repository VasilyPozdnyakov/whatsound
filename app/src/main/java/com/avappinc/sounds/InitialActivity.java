package com.avappinc.sounds;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import static com.avappinc.sounds.Music.musicPool;
import static com.avappinc.sounds.SettingsActivity.STUDY_SWITCH;

public class InitialActivity extends AppCompatActivity {

    public static final String SECTION_NAME = "SECTION_NAME";
    public static final String CURRENT_LEVEL = "CURRENT_LEVEL";

    public static ElementsLists activityElementsLists = new ElementsLists();

    private ImageView categories, game, settings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial);

        categories = findViewById(R.id.categoryView);
        game = findViewById(R.id.gameView);
        settings = findViewById(R.id.settingsView);

        initMusicPool();
        activityElementsLists = createAndFillElementsLists();

        categories.setOnClickListener(v -> openCategoriesActivity());
        game.setOnClickListener(
                v -> {
                    Music.INSTANCE.pauseMainMusic();
                    if (!activityElementsLists.getGuessList().isEmpty()) {
                        openGuessActivity();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(this);
                        builder.setCancelable(true);
                        builder.setTitle(R.string.toolessDialogTitle);
                        builder.setMessage(R.string.toolessElements);
                        builder.setPositiveButton(R.string.ok, (dialog, which) -> {
                        });
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }
        );
        settings.setOnClickListener(v -> openSettingsActivity());
    }

    @Override
    public void finish() {
        super.finish();
        musicPool.release();
        activityElementsLists.saveGuessList(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.INSTANCE.pauseMainMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Music.INSTANCE.playMusic(this);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!preferences.getBoolean(STUDY_SWITCH, false)) {
            activityElementsLists.setGuessList(InitialActivity.activityElementsLists.getRandomList());
        } else {
            activityElementsLists.loadGuessActivity(this);
        }
    }

    private void initMusicPool() {
        Music.INSTANCE.initMusic(this);
        Music.INSTANCE.playMusic(this);
    }

    public void openCategoriesActivity() {
        Intent intent = new Intent(this, CategoriesActivity.class);
        startActivity(intent);
    }

    public void openGuessActivity() {
        Intent intent = new Intent(this, GuessActivity.class);
        intent.putExtra(CURRENT_LEVEL, Level.EASY.name());
        startActivity(intent);
    }

    public void openSettingsActivity() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }


    private ElementsLists createAndFillElementsLists() {
        final ElementsLists elementsLists = new ElementsLists();
        elementsLists.setHomeList(createListOfActivityElements(R.array.home));
        elementsLists.setCarList(createListOfActivityElements(R.array.cars));
        elementsLists.setAnimalList(createListOfActivityElements(R.array.animals));
        elementsLists.setNatureList(createListOfActivityElements(R.array.nature));
        elementsLists.setMusicList(createListOfActivityElements(R.array.music));
        ArrayList<ActivityElement> allCategoriesList = new ArrayList<>();
        allCategoriesList.addAll(elementsLists.getHomeList());
        allCategoriesList.addAll(elementsLists.getCarList());
        allCategoriesList.addAll(elementsLists.getAnimalList());
        allCategoriesList.addAll(elementsLists.getNatureList());
        allCategoriesList.addAll(elementsLists.getMusicList());
        elementsLists.setRandomList(allCategoriesList);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (!preferences.getBoolean(STUDY_SWITCH, false)) {
            elementsLists.setGuessList(allCategoriesList);
        } else {
            elementsLists.loadGuessActivity(this);
        }
        return elementsLists;
    }

    private List<ActivityElement> createListOfActivityElements(int resourceArray) {
        List<ActivityElement> list = new ArrayList<>();
        TypedArray typedArray = getResources().obtainTypedArray(resourceArray);
        for (int i = 0; i < typedArray.length(); i++) {
            int resourceId = typedArray.getResourceId(i, 0);
            ActivityElement element = ActivityElement.getElementByTypedArray(getResources().obtainTypedArray(resourceId));
            list.add(element);
        }
        return list;
    }

}