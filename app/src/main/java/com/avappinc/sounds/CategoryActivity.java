package com.avappinc.sounds;

import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.Collections;
import java.util.List;

import static com.avappinc.sounds.InitialActivity.SECTION_NAME;
import static com.avappinc.sounds.InitialActivity.activityElementsLists;
import static com.avappinc.sounds.Music.musicPool;
import static com.avappinc.sounds.SettingsActivity.STUDY_SWITCH;

public class CategoryActivity extends AppCompatActivity {

    private int currentIndex = 0;
    private ImageView image, nextButton, prevButton;
    private TextView text;
    private boolean musicPlaying = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        image = findViewById(R.id.allView);
        nextButton = findViewById(R.id.nextView);
        prevButton = findViewById(R.id.prevView);
        text = findViewById(R.id.nameTextView);

        Category category = Category.RANDOM;
        Bundle extras = getIntent().getExtras();
        ElementsLists elementsList = InitialActivity.activityElementsLists;
        if (extras != null) {
            category = Category.valueOf(extras.getString(SECTION_NAME));
        }

        final List<ActivityElement> activityElementList = getActivityElementList(elementsList, category);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        image.setImageDrawable(getResources().getDrawable(activityElementList.get(currentIndex).getImage()));
        text.setText(activityElementList.get(currentIndex).getName());

        image.setOnClickListener(
                v -> {
                    if (musicPlaying) {
                        return;
                    }
                    musicPool.release();
                    musicPool = MediaPlayer.create(this, activityElementList.get(currentIndex).getSound());
                    musicPool.setOnPreparedListener(mp -> {
                        musicPool.start();
                        musicPlaying = true;
                    });
                    musicPool.setOnCompletionListener(mp -> {
                        musicPool.release();
                        musicPlaying = false;
                    });
                    if (preferences.getBoolean(STUDY_SWITCH, false) && !elementsList.getGuessList().contains(activityElementList.get(currentIndex))) {
                        elementsList.getGuessList().add(activityElementList.get(currentIndex));
                        elementsList.saveGuessList(this);
                    }
                }
        );
        nextButton.setOnClickListener(
                v -> {
                    currentIndex = currentIndex + 1;
                    if (currentIndex > activityElementList.size() - 1) {
                        currentIndex = 0;
                    }
                    image.setImageDrawable(getResources().getDrawable(activityElementList.get(currentIndex).getImage()));
                    text.setText(activityElementList.get(currentIndex).getName());
                    Music.INSTANCE.pauseMusic();
                    musicPlaying = false;
                }
        );
        prevButton.setOnClickListener(
                v -> {
                    currentIndex = currentIndex - 1;
                    if (currentIndex < 0) {
                        currentIndex = activityElementList.size() - 1;
                    }
                    image.setImageDrawable(getResources().getDrawable(activityElementList.get(currentIndex).getImage()));
                    text.setText(activityElementList.get(currentIndex).getName());
                    Music.INSTANCE.pauseMusic();
                    musicPlaying = false;
                }
        );
    }

    @Override
    protected void onStop() {
        super.onStop();
        Music.INSTANCE.pauseMusic();
    }

    @Override
    public void finish() {
        super.finish();
        activityElementsLists.saveGuessList(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.INSTANCE.pauseMusic();
    }

    private List<ActivityElement> getActivityElementList(ElementsLists elementsLists, Category category) {
        List<ActivityElement> list;
        switch (category) {
            case HOME:
                list = elementsLists.getHomeList();
                break;
            case CAR:
                list = elementsLists.getCarList();
                break;
            case ANIMAL:
                list = elementsLists.getAnimalList();
                break;
            case NATURE:
                list = elementsLists.getNatureList();
                break;
            case MUSIC:
                list = elementsLists.getMusicList();
                break;
            default:
                list = elementsLists.getRandomList();
        }
        Collections.shuffle(list);
        return list;
    }

}
