package com.avappinc.sounds;

import android.content.res.TypedArray;

public class ActivityElement {

    private static final int NAME_INDEX = 0;
    private static final int IMAGE_INDEX = 1;
    private static final int SOUND_INDEX = 2;

    private String name;
    private int image;
    private int sound;

    public ActivityElement(String name, int image, int sound) {
        this.name = name;
        this.image = image;
        this.sound = sound;
    }

    public static ActivityElement getElementByTypedArray(TypedArray typedArray) {
        String name = typedArray.getString(NAME_INDEX);
        int image = typedArray.getResourceId(IMAGE_INDEX, 0);
        int sound = typedArray.getResourceId(SOUND_INDEX, 0);
        return new ActivityElement(name, image, sound);
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }

    public int getSound() {
        return sound;
    }

}
