package com.avappinc.sounds;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.avappinc.sounds.SettingsActivity.STUDY_SWITCH;

public class ElementsLists {

    private List<ActivityElement> animalList = new ArrayList<>();
    private List<ActivityElement> carList = new ArrayList<>();
    private List<ActivityElement> natureList = new ArrayList<>();
    private List<ActivityElement> musicList = new ArrayList<>();
    private List<ActivityElement> homeList = new ArrayList<>();
    private List<ActivityElement> randomList = new ArrayList<>();
    private List<ActivityElement> guessList = new ArrayList<>();

    public List<ActivityElement> getAnimalList() {
        if (animalList == null) {
            animalList = new ArrayList<>();
        }
        return animalList;
    }

    public void setAnimalList(List<ActivityElement> animalList) {
        this.animalList = animalList;
    }

    public List<ActivityElement> getCarList() {
        if (carList == null) {
            carList = new ArrayList<>();
        }
        return carList;
    }

    public void setCarList(List<ActivityElement> carList) {
        this.carList = carList;
    }

    public List<ActivityElement> getNatureList() {
        if (natureList == null) {
            natureList = new ArrayList<>();
        }
        return natureList;
    }

    public void setNatureList(List<ActivityElement> natureList) {
        this.natureList = natureList;
    }

    public List<ActivityElement> getMusicList() {
        if (musicList == null) {
            musicList = new ArrayList<>();
        }
        return musicList;
    }

    public void setMusicList(List<ActivityElement> musicList) {
        this.musicList = musicList;
    }

    public List<ActivityElement> getHomeList() {
        if (homeList == null) {
            homeList = new ArrayList<>();
        }
        return homeList;
    }

    public void setHomeList(List<ActivityElement> homeList) {
        this.homeList = homeList;
    }

    public List<ActivityElement> getRandomList() {
        if (randomList == null) {
            randomList = new ArrayList<>();
        }
        return randomList;
    }

    public void setRandomList(List<ActivityElement> randomList) {
        this.randomList = randomList;
    }

    public List<ActivityElement> getGuessList() {
        if (guessList == null) {
            guessList = new ArrayList<>();
        }
        return guessList;
    }

    public void setGuessList(List<ActivityElement> guessList) {
        this.guessList = guessList;
    }

    public void resetGuessList() {
        this.guessList = new ArrayList<>();
    }

    public void loadGuessActivity(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        Set<String> stringSet = preferences.getStringSet(context.getString(R.string.guessList), new HashSet<>());
        resetGuessList();
        if (!stringSet.isEmpty()) {
            for (ActivityElement element : getRandomList()) {
                if (stringSet.contains(element.getName()) && !getGuessList().contains(element)) {
                    getGuessList().add(element);
                }
            }
        }
    }

    public void saveGuessList(Context context) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (!preferences.getBoolean(STUDY_SWITCH, false)) {
            return;
        }
        Set<String> values = new HashSet<>();
        SharedPreferences.Editor editor = preferences.edit();
        for (ActivityElement element : getGuessList()) {
            String name = element.getName();
            values.add(name);
        }
        editor.putStringSet(context.getString(R.string.guessList), values);
        editor.apply();
    }
}
