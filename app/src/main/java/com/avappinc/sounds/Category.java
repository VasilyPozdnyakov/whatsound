package com.avappinc.sounds;

public enum Category {
    HOME,
    CAR,
    ANIMAL,
    RANDOM,
    NATURE,
    MUSIC
}
