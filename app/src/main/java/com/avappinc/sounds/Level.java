package com.avappinc.sounds;

public enum Level {
    EASY,
    MEDIUM,
    HARD
}
