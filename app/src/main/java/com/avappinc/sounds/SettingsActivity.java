package com.avappinc.sounds;

import static com.avappinc.sounds.InitialActivity.activityElementsLists;
import static com.avappinc.sounds.Music.mediaPlayer;

import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.widget.Toast;

public class SettingsActivity extends PreferenceActivity {

    public static final String SOUND_SWITCH = "soundSwitch";
//    public static final String VIBRATION_SWITCH = "vibrationSwitch";
    public static final String STUDY_SWITCH = "studySwitch";
    public static final String DELETE_SWITCH = "deleteSwitch";

    private SwitchPreference soundSwitch;
    //    private SwitchPreference vibrationSwitch;
    private SwitchPreference studySwitch;
    private SwitchPreference deleteSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_Setting);
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);

        soundSwitch = (SwitchPreference) findPreference(SOUND_SWITCH);
//        review = (EditTextPreference) findPreference(REVIEW_SWITCH);
//        vibrationSwitch = (SwitchPreference) findPreference(VIBRATION_SWITCH);
        studySwitch = (SwitchPreference) findPreference(STUDY_SWITCH);
        deleteSwitch = (SwitchPreference) findPreference(DELETE_SWITCH);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.getBoolean(SOUND_SWITCH, true)) {
            soundSwitch.setIcon(android.R.drawable.ic_lock_silent_mode_off);
        } else {
            soundSwitch.setIcon(android.R.drawable.ic_lock_silent_mode);
        }

        if (preferences.getBoolean(STUDY_SWITCH, false)) {
            deleteSwitch.setEnabled(true);
        }

        Preference button = findPreference("BUT");
        button.setOnPreferenceClickListener(preference -> {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.avappinc.sounds")));
            return true;
        });

        soundSwitch.setOnPreferenceChangeListener((preference, newValue) -> {
            if (newValue instanceof Boolean && (Boolean) newValue) {
                mediaPlayer = MediaPlayer.create(this, R.raw.music);
                mediaPlayer.setLooping(true);
                mediaPlayer.setOnPreparedListener(mp -> mediaPlayer.start());
                soundSwitch.setIcon(android.R.drawable.ic_lock_silent_mode_off);
            } else {
                Music.INSTANCE.pauseMainMusic();
                soundSwitch.setIcon(android.R.drawable.ic_lock_silent_mode);
            }
            return true;
        });


        studySwitch.setOnPreferenceChangeListener((preference, newValue) -> {
            if (newValue instanceof Boolean) {
                deleteSwitch.setEnabled((Boolean) newValue);
                deleteSwitch.setChecked(false);
                if ((Boolean) newValue) {
                    activityElementsLists.loadGuessActivity(this);
                }
            }
            return true;
        });


        deleteSwitch.setOnPreferenceChangeListener((preference, newValue) -> {
            if (newValue instanceof Boolean && (Boolean) newValue) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setCancelable(true);
                builder.setTitle(R.string.deleteDialogTitle);
                builder.setMessage(R.string.deleteDialogMessage);
                builder.setPositiveButton(R.string.deleteDialogPositiveButton, (dialog, which) -> {
                    deleteSwitch.setChecked(false);
                    activityElementsLists.resetGuessList();
                    activityElementsLists.saveGuessList(this);
                    Toast toast = Toast.makeText(getApplicationContext(), R.string.deleteDialogToast, Toast.LENGTH_SHORT);
                    toast.show();
                });
                builder.setNegativeButton(R.string.deleteDialogNegativeButton, (dialog, which) -> deleteSwitch.setChecked(false));

                AlertDialog dialog = builder.create();
                dialog.show();
            }
            return true;
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        Music.INSTANCE.pauseMainMusic();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Music.INSTANCE.playMusic(this);
    }

    @Override
    public void finish() {
        super.finish();
        activityElementsLists.saveGuessList(this);
    }

}
